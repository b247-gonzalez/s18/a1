/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


// Part 1: Start
function addition(addends1, addends2) {
    let sum = addends1 + addends2;
    console.log("Displayed sum of 5 and 15");
    console.log(sum);
}

addition(5,15);

function subtraction(minuend, subrahend) {
    let difference = minuend - subrahend;
    console.log("Displayed difference of 20 and 5");
    console.log(difference);
}

subtraction(20,5);
// Part 1: End

// Part 2: Start
function multiplication(multiplicand, multiplier) {
    let prod = multiplicand * multiplier;
    console.log("The product of " + multiplicand + " and " + multiplier + ":");
    return prod;
}

let product = multiplication(50,10);
console.log(product);

function division(dividend, divisor) {
    let quo = dividend / divisor;
    console.log("The quotient of " + dividend + " and " + divisor + ":");
    return quo;
}

let quotient = division(50,10);
console.log(quotient);
// Part 2: End

// Part 3: Start
function areaOfCircle(radius) {
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    return (Math.PI * (radius*radius));
}
let circleArea = areaOfCircle(15);
console.log(circleArea);
// Part 3: End

// Part 4: Start
function average(g1, g2, g3, g4) {
    console.log("The average of " + g1 + ", " + g2 + ", " + g3 + ", and " + g4 + ":");
    return (g1 + g2 + g3 + g4) / 4;
}

let averageVar = average(20,40,60,80);
console.log(averageVar);
// Part 4: End

// Part 5: Start
function passOrFail(score, total) {
    let grade = (score/total) * 100;
    console.log("Is " + score + "/" + total + " a passing score?");
    return grade >= 75;
}

let isPassed = passOrFail(38,50);
console.log(isPassed);
// Part 5: End